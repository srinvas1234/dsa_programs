package com.app.strings;

public class StringRefEqualComparison {

	public static void main(String[] args) {
		
		String s1=new String("Srinivas");
		String s2=new String("Srinivas");
		
		String s3=s1;
		
		System.out.println(s1==s2);//false
		System.out.println(s1.equals(s2)); //true
		
		System.out.println(s1==s3);//true
		System.out.println(s1.equals(s3));//true
		
	}
}
