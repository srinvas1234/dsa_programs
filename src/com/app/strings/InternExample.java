package com.app.strings;

public class InternExample {

	public static void main(String[] args) {
		String s1=new String("abc");
		String s2=s1.intern();
		String s3="abc";
		
		System.out.println(s1==s2);//false
		System.out.println(s2==s1);//false
		System.out.println(s2==s3);//true
	}
}
