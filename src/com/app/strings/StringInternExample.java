package com.app.strings;

public class StringInternExample {
public static void main(String[] args) {
	
	
	String s1=new String("abc");
	String s2=s1.concat("def");
	System.out.println(s2);//abcdef
	String s3=s2.intern();
	System.out.println(s3);//abcdef
	String s4="abcdef";
	System.out.println(s4);//abcdef
	System.out.println(s3==s4);//true
	
}
}
