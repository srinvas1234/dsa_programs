package com.app.strings;

public class StringBufferExample
{
public static void main(String[] args) {

	StringBuffer sb1=new StringBuffer("abcd");
	StringBuffer sb2=new StringBuffer("ABCD");
	
	String s1=new String(sb1);
	String s2=new String(sb2);
	
	System.out.println(sb1);
	System.out.println(sb2);
	
	System.out.println(s1);
	System.out.println(s2);
	
	System.out.println();
	
	String sb11=new String("abcd");
	String sb21=new String("ABCD");
	
	StringBuffer s11=new StringBuffer(sb1);
	StringBuffer s21=new StringBuffer(sb2);
	
	System.out.println(sb11);
	System.out.println(sb21);
	
	System.out.println(s11);
	System.out.println(s21);
}
}
