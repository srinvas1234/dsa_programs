package com.app.strings;

public class StringUpperLowerCaseCheck {
public static void main(String[] args) {
	
	String s1=new String("i love my india");
	String s2=new String("i love my india");
	
	System.out.println(s1==s2);//false --1
	
	String s3="i love my india";
	
	System.out.println(s1==s3);//false--2
	
	
	String s4="i love my india";
	
	System.out.println(s3==s4);//true--3
	
	String s5="i love "+"my india";
	 System.out.println(s4==s5);//true--4
	 
	 String s6="i love ";
	 String s7=s6+"my india";
	 
	 System.out.println(s4==s7);//false--5
	 
	 final String s8="i love ";
	 String s9=s8+"my india";
	 
	 System.out.println(s4==s9);//true--6
	 
	 
	 
	
}
}
