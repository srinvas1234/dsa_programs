package com.app.strings;

public class StringImMutableExample {

	
	/*
	 * String ---------------> immutable obj StringBuffer ---------> mutable obj at
	 * a time only one obj is allowed StringBuilder --------> mutable obj at a time
	 * any number of obj's allowed StringTokenizer ------> divide the given str into
	 * tokens
	 * 
	 * Reference and Content comparision ---------------------------------
	 * primitives --------> == for content comparision, we dn't have reference
	 * comparision objects -----------> == for ref comparision and .equals() for ref
	 * comprision
	 */
	public static void main(String[] args) {
		
		String s=new String("Srinivas");
		s.concat("Vadla");
		System.out.println(s);
	}
}
