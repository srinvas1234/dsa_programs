package com.app.strings;

public class StringEqualAndIgnoreCase {

	public static void main(String[] args) {
		
		System.out.println("abc".equals("abc"));
		System.out.println("abc".equals("BAC"));
		
		System.out.println("abc".equalsIgnoreCase("ABC"));
		System.out.println("abc".equals("BBB"));
	}
}
