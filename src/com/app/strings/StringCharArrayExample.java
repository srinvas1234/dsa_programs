package com.app.strings;

import java.util.Arrays;

public class StringCharArrayExample {

	public static void main(String[] args) {
		
		String s=new String("srinivas");
		
		System.out.println(s);
		
		byte[] b=s.getBytes();
		
		System.out.println(Arrays.toString(b));
		
		
	}
}
