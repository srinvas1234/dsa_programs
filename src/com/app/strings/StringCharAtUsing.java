package com.app.strings;

public class StringCharAtUsing {

	public static void main(String[] args) {
		
		
		String s=new String("abc");
		
		System.out.println(s);
		
		System.out.println(s.charAt(0));
		System.out.println(s.charAt(1));
		System.out.println(s.charAt(2));
		//System.out.println(s.charAt(3));//Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range: 3
		
	}
}
