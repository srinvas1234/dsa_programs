package com.app.strings;

public class StringCharArrayEx {

	public static void main(String[] args) {
		
		
		char ch[]= {'w','e','l','c','o','m','e'};
		//           0   1   2   3   4   5   6

		String s1=new String(ch);
		String s2=new String(ch,3,4);
		String s3=new String(ch,0,2);
		
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		
		
	}
}
