package com.app;

import java.util.Scanner;

public class MaxNumberabc {

	public static int max1(int a,int b,int c)
	{
		return (a>b&&a>c)?a:(b>c?b:a);
	}
	
	public static int max2(int a,int b,int c)
	{
		return Math.max(Math.max(a, b),c);
	}
	
}
class Test{
	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the 1st Number: ");
		int a=sc.nextInt();
		System.out.println("Enter the 2nd Number: ");
		int b=sc.nextInt();
		System.out.println("Enter the 3rd Number: ");
		int c=sc.nextInt();
		
		System.out.println(MaxNumberabc.max1(a,b,c));
		System.out.println(MaxNumberabc.max2(a,b,c));
	}

}
