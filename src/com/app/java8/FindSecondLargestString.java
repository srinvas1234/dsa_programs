package com.app.java8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FindSecondLargestString {

	public static void main(String[] args) {
		
		List<String> ss=Arrays.asList("I", "am","good","programmer","sss");
				
		System.out.println(ss);
		
		
		
		String sss=ss.stream().distinct().sorted(Comparator.reverseOrder()).limit(2).skip(1).findFirst().get();
		
		System.out.println(sss);
	}
}
