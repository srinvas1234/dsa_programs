package com.app.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {

	public static void main(String[] args) {
		
		ArrayList<Integer> al=new ArrayList<Integer>(Arrays.asList(10,20,30,40,50));
		System.out.println(al);
		Stream s=al.stream();
		//Stream s1=s.filter(Predicate);
		/***
		 * Stream -->It is an interface ,present in java.util.stream
		 * 
		 * we got stream by invoke stream() on top of collection object
		 * 
		 * we can process a object in 2 ways
		 * a)configuration
		 * 
		 * i)filter >>>to filter object on basis of boolean condition[T/F)
		 * ii)map >>>if we want to make two object for every present in collection
		 * map(), filter(), distinct(), sorted(), limit(), skip()


		 * 
		 * b)processing
		 * 
		 * forEach(), toArray(), reduce(), collect(), min(),
		 *  max(), count(), anyMatch(), allMatch(), noneMatch(), findFirst(), findAny()
//nonematch ??? if no element matched it return true

			*/
		
		
		//using filter method >>> greater than 20
		List<Integer> collect=al.stream().filter(i->i>20).collect(Collectors.toList());
		System.out.println(collect);
		//adding 5 for each element using map 
		
		List<Integer> collect1=al.stream().map(i->i+5).collect(Collectors.toList());
		System.out.println(collect1);
		//Terminal operations
		
		//toArray()
		
		
		 Stream<Integer> ss=Stream.of(1,2,3,4,5); Object[] ob=ss.toArray();
		 
		 for (Object object : ob) { System.out.print(object+" "); }
		 System.out.println("-------------------");
		 
		Stream<Integer> ss1=Stream.of(1,2,3,4,5); 
	long count=ss1.count();
		System.out.println(count);
		
		System.out.println();
		
		Stream<Integer> ss2=Stream.of(1,2,3,4,5); 
		
		ss2.forEach(ele->System.err.print(ele+" "));
		
		
		System.out.println();
	//min and max
		Stream<Integer> ss3=Stream.of(1,2,3,4,5);
		Stream<Integer> ss4=Stream.of(1,2,3,4,5);
		Optional<Integer>min=ss3.min((o1,o2)->o1.compareTo(o2));
		Optional<Integer>max=ss4.max((o1,o2)->o1.compareTo(o2));
		System.out.println("min "+min.get());
		System.out.println("max "+max.get());
		
		//anyMath
		
		List<Integer> listOfNum=Arrays.asList(22,33,411,3,3,23,334,12);
		boolean b=listOfNum.stream().anyMatch(i->i==22);
		System.out.println(b);
		
		//anyMath
		System.out.println();
		List<Integer>listOfNums= Arrays.asList(22,33,411,3,3,23,334,12);
		boolean bb=listOfNums.stream().allMatch(element->element>0);
		System.out.println(bb);
		
		//findAny
		List<Integer>findanyelement= Arrays.asList(8,22,33,411,3,3,23,334,12);
Integer ss44=findanyelement.stream().findAny().get();
System.out.println(ss44);
		
		System.out.println("***************");
		System.out.println();
		List<Integer>findanyelement1= Arrays.asList(22,33,411,3,3,23,334,12);
		Integer ss45=findanyelement1.stream().findFirst().get();
				System.out.println(ss45);
	}
}
