package com.app.java8;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DuplicateArrays {
public static void main(String[] args) {
	
	
	/*
	 * numbers.stream().filter(i -> Collections.frequency(numbers, i) >1)
	 * .collect(Collectors.toSet()).forEach(System.out::println);
	 */
	Integer[] numbers = new Integer[] { 2,3,4,5,6,7,8,9,8,4,4 };
	Set<Integer> allItems = new HashSet<>();
	Set<Integer> duplicates = Arrays.stream(numbers)
	        .filter(n -> !allItems.add(n)) //Set.add() returns false if the item was already in the set.
	        .collect(Collectors.toSet());
	System.out.println(duplicates); // [1, 4]
	
List<Integer> jj=Arrays.asList(1, 1, 2, 3, 3, 3, 4, 5, 6, 6, 6, 7, 8);

List<Integer> hh=jj.stream().distinct().collect(Collectors.toList());
System.out.println(hh);
	
}
}
