package com.app.java8;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.event.TableColumnModelListener;

public class Test {

	public static void main(String[] args) {
		
		
		
		List<Employee> ee=Stream.of(
				new Employee(122, "srinivas", "cse", 67677.5),
				new Employee(123, "raju", "cse", 7677.5),
				new Employee(125, "nihas", "cse", 677.5),
				new Employee(127, "sneha", "cse", 6997.5))
				.collect(Collectors.toList());
		
		
		Comparator<Employee> compareBySalary=Comparator.comparing(Employee::getSalary);
		
		Map<String, Optional<Employee>> eeeee=ee.stream().collect(Collectors.groupingBy(Employee::getDept,Collectors.reducing(BinaryOperator.maxBy(compareBySalary))));
		

		
		System.out.println(eeeee);
		
	
		
		System.out.println("*********************");
		Map<String, List<Employee>> tt=ee.stream().collect(Collectors.groupingBy(Employee::getDept));
		System.out.println(tt);
		
		
		List<Employee> ss=ee.stream().filter(e->e.getSalary()>1000).collect(Collectors.toList());
		System.out.println();
		System.out.println(ss);
		System.out.println("***************");
		ss.forEach(System.out::println);
		
		
		
		
		
		
		
		System.out.println("************888888");
		
		Set deptSet=ee.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Employee::getDept))));
		deptSet.size();
		System.out.println(deptSet);
		
		
		
		System.out.println("((((");
		Comparator<Employee> sss=Comparator.comparing(Employee::getSalary);
		
		Map<Integer, Optional<Employee>> hj=ee.stream().collect(Collectors.groupingBy(Employee::getEmpId,Collectors.reducing(BinaryOperator.maxBy(sss))));
		System.out.println(hj);
		
		
		
		
		
		
	}
}
