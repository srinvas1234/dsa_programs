package com.app.java8;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateCharacterUsingJava8 {


	public static void main(String[] args) {
	
		
		/*
		 * String input="srinivass";
		 * 
		 * Map<String,Long> countMap=Arrays.stream(input.split(""))
		 * .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		 * System.out.println(countMap);
		 */
		
		String input="srinu";
		
		Map<String,Long> countMap=Arrays.stream(input.split("")).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		System.out.println(countMap);
		
		System.out.println();
		String input1="srinivas";
		Map<String, Long> countMap1=Arrays.stream(input1.split("")).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		System.out.println(countMap1);
		
		
	}
}
