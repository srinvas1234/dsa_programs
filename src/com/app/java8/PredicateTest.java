package com.app.java8;

import java.util.function.Predicate;

public class PredicateTest {

	public static void main(String[] args) {
		
		Predicate<Integer> i=ii->ii>250;
		System.out.println(i.test(100));
		
		Predicate<String> pp=pp1->pp1.length()>5;
		System.out.println(pp.test("srinivas"));
		System.out.println();
		
		int a[]= {1,2,3,4,5,6};
		Predicate<Integer> ppp=pppp->pppp%2==0;
		Predicate<Integer> p55=p44->p44>2;	
		System.out.println("Print all even nunber");
		m1(ppp,a);
		System.out.println("Greater than 2 ");
		m1(p55,a);
		System.out.println("Print all odd nunber");
		m1(ppp.negate(),a);
		
		System.out.println("Greater than 2 or ");
		m1(p55.or(ppp),a);
		
		System.out.println("Greater than 2 and");
		m1(p55.and(ppp),a);
		
		
	}
	
	
	public static void m1(Predicate<Integer> p,int[] x)
	{
		for(int i:x)
		{
			if(p.test(i))
			{
				System.out.println(i);
			}
		}
	}
	
	
}
