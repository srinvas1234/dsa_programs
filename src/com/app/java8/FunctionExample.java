package com.app.java8;

import java.util.function.Function;


//Write a function to return String length
public class FunctionExample {

	public static void main(String[] args) {
		
		Function<String, Integer> i=s->s.length();
		System.out.println(i.apply("srinivas"));
		System.out.println(i.apply("raju"));
	}
}
