package com.app.reverse;

public class ReverseStringExample {

	public static void main(String[] args) {
		
		String s="the quick brown fox jumps over the lazy dog";
		
		System.out.println(s);
		
		System.out.println(new StringBuffer(s).reverse());
	}
}
