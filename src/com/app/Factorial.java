package com.app;

import java.util.Scanner;

public class Factorial {

	
	public static int fact1(int n)
	{
		int fact=1;
		for(int i=1;i<=n;i++) {
			fact=fact*i;
		}
		return fact;
	}
	public static int fact2(int n)
	{
		if(n==0)
		{
			return 1;
		}
		else {
			return n*fact2(n-1);
		}
	}
	
}
class Test9{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the factorial: ");
		int n=sc.nextInt();
		System.out.println(Factorial.fact1(n));
		System.out.println(Factorial.fact2(n));
	}
}