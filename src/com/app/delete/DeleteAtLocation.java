package com.app.delete;

import java.util.Arrays;

public class DeleteAtLocation {

	public static int[] deleteAtLocation(int[] a,int loc)
	{
		int k,b[]=new int[a.length-1];
		int i;
		
		for(i=0,k=0;i<a.length;i++)
		{
			if(i==loc)
			{
				continue;
			}else {
				b[k++]=a[i];
			}
		}
		return b;
		
		
	}
	
	
	
}
class Test77{
	public static void main(String[] args) {
		int a[]= {10,11,12,13,14,15,16};
		System.out.println(Arrays.toString(a));
		a=DeleteAtLocation.deleteAtLocation(a,2);
		System.out.println(Arrays.toString(a));
	}
}