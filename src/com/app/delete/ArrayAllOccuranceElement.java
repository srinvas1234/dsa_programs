package com.app.delete;

import java.util.Arrays;

public class ArrayAllOccuranceElement {

	public static int[] deleteAtLocation(int[] a,int loc)
	{
		int k,i,b[]=new int[a.length-1];
		
		for(i=0,k=0;i<a.length;i++)
		{
			if(i==loc)
			{
				continue;
			}
			else {
				b[k++]=a[i];
			}
		}
		return b;
	}
	
	public static int[] deleteElement(int[] a,int element)
	{
		int index=-1,i,k,c=0;
		
		for(i=0;i<a.length;i++)
		{
			if(a[i]==element)
			{
				a=deleteAtLocation(a, i);
			}
		}
		
		
		return a;
		
		
		
	}
	
	
	
}
class Test6663{
	public static void main(String[] args) {
		
	int a[]= {10, 11, 12, 13, 12, 15, 12};
	System.out.println(Arrays.toString(a));
	a=ArrayAllOccuranceElement.deleteElement(a,12);
	System.out.println(Arrays.toString(a));
	}
}