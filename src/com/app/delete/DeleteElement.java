package com.app.delete;

import java.util.Arrays;

public class DeleteElement {

	public static int[] deleteElement(int[] a,int element)
	{
		int index=-1,i,k;
		
		
		for(i=0;i<a.length;i++)
		{
			if(a[i]==element)
			{
				index=i;
				break;
			}
		}
		if(index!=-1)
		{
			int b[]=new int[a.length-1];
			for(i=0,k=0;i<a.length;i++)
			{
				if(i==index)
					
				{
					continue;
				}else {
					b[k++]=a[i];
				}
			}
			return b;
		}
		return a;
	}
	
	
	
}
class Test67788{
	public static void main(String[] args) {
		
		int a[]= {10,11,12,13,14,15,16};
		
		System.out.println(Arrays.toString(a));
		a=DeleteElement.deleteElement(a,13);
		System.out.println(Arrays.toString(a));
		
	}
}