package com.app.stringbuffer;

public class StringBufferExample344 {

	public static void main(String[] args) {
		
		StringBuffer sb=new StringBuffer("welcome");
		System.out.println(sb);//welcome
		System.out.println(sb.length());
		System.out.println(sb.capacity());
		System.out.println("********");
		sb.setLength(2);
		System.out.println(sb);
		System.out.println(sb.length());
		System.out.println(sb.capacity());
		System.out.println("********");
		sb.trimToSize();
		System.out.println(sb);
		System.out.println(sb.length());
		System.out.println(sb.capacity());
		System.out.println("********");
		sb.ensureCapacity(55);
		System.out.println(sb);
		System.out.println(sb.length());
		System.out.println(sb.capacity());
		
		
	}
}
