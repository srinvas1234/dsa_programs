package com.app.stringbuffer;

public class StringBufferCharAt {

	public static void main(String[] args) {
		StringBuffer sb=new StringBuffer("welkomae");
		System.out.println(sb);
		System.out.println(sb.charAt(3));
		System.out.println(sb);
		sb.setCharAt(3, 'c');
		System.out.println(sb);
		sb.deleteCharAt(6);
		System.out.println(sb);
	}
}
