package com.app.stringbuffer;

public class StringBufferSecondExample {

	
	public static void main(String[] args) {
		StringBuffer sb=new StringBuffer("abc");
		System.out.println(sb);//abc
		System.out.println(sb.length());//3
		System.out.println(sb.capacity());//19
	}
}
