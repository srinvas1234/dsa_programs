package com.app.stringbuffer;

public class StringBufferInsertData {
public static void main(String[] args) {
	
	StringBuffer sb = new StringBuffer();
	System.out.println(sb);//
	sb.append("python ");
	sb.append("and ");
	sb.append("java ");
	sb.append("programming");
	System.out.println(sb);//python and java programming
	sb.insert(0,"welcome ");
	System.out.println(sb);//welcome python and java programming
	sb.insert(8,"to ");
	System.out.println(sb);//welcome to python and java programming
	sb.delete(11,22);// 11th char to 21st char
	System.out.println(sb);//welcome to java programming
}
}
