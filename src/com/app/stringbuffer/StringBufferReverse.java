package com.app.stringbuffer;

public class StringBufferReverse {

	public static void main(String[] args) {
		
		StringBuffer sb=new StringBuffer("welcome");
		System.out.println(sb);
		sb.reverse();
		System.out.println(sb);
	}
}
