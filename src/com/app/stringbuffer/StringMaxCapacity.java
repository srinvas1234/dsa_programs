package com.app.stringbuffer;

public class StringMaxCapacity {

	public static void main(String[] args) {
		
		StringBuffer sb=new StringBuffer();
		System.out.println(sb);
		System.out.println(sb.length());//0
		System.out.println(sb.capacity());//16
		System.out.println("*************");
		sb.append("abcdefghijklmnop");//abcdefghijklmnop
		System.out.println(sb);//16
		System.out.println(sb.length());//16
		System.out.println(sb.capacity());//16
		sb.append("q");
		System.out.println(sb);//abcdefghijklmnopq
		System.out.println(sb.length());//17
		System.out.println(sb.capacity());//34
		
	}
}
