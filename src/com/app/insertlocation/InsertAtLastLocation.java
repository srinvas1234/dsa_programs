package com.app.insertlocation;

import java.util.Arrays;

public class InsertAtLastLocation {

	
	public static int[] insertAtLastLocation(int[] a,int element)
	{
		
		int i,b[]=new int[a.length+1];
		
		for(i=0;i<a.length;i++)
		{
			b[i]=a[i];
		}
		b[i]=element;
		return b;
		
	}
	
}
class Test456{
	public static void main(String[] args) {
		int a[]= {3,7,9};
		
		System.out.println(Arrays.toString(a));
		a=InsertAtLastLocation.insertAtLastLocation(a,999);
		a=InsertAtLastLocation.insertAtLastLocation(a,888);
		System.out.println(Arrays.toString(a));
		}
	}
