package com.app.insertlocation;

import java.util.Arrays;

public class InsertAtLocation {

	
	public static int[] insertAtLocation(int a[],int element,int index)
	{

		int i,b[]=new int[a.length+1];
		//logic
		
		for(i=0;i<index;i++)
		{
			b[i]=a[i];
		}
		b[index]=element;
		
		for(;i<a.length;i++)
			
		{
			b[i+1]=a[i];
		}
		
		return b;
		
		
	}
}

class Test4433{
	public static void main(String[] args) {
		
		int[] a= {3,7,9};
		System.out.println(Arrays.toString(a));
		
		a=InsertAtLocation.insertAtLocation(a,999,2);
		System.out.println(Arrays.toString(a));
		 
		a=InsertAtLocation.insertAtLocation(a,888,1);
		System.out.println(Arrays.toString(a));
	}
}