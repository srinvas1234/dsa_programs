package com.app.insertlocation;

import java.util.Arrays;

public class InsertAtFirstLocation {

	
	
	public static int[] insertFirst(int[] a,int element)
	{
		int i,b[]=new int[a.length+1];
		
		//logic
		
		b[0]=element;
		
		for(i=0;i<a.length;i++)
		{
			b[i+1]=a[i];
		}
		return b;
		
		
	}
	
	
		
	}
class Test5465{
	public static void main(String[] args) {
		int a[]= {3,7,9};
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(InsertAtFirstLocation.insertFirst(a,999)));
	}
}