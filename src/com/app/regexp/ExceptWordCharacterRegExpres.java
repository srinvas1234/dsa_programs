package com.app.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Regular Expressions and its applications
--------------------------------------------
a group of strings according to a particular pattern or format is called as regular exp

steps to prepare re object
--------------------------
1) import java.util.regex.*;
2) pattern object ------------> format for data
3) matcher object ------------> target string is in format

Validate Mobile Number:
-----------------------
	123 ------------> No
	1234567890 -----> No
	9123456789 -----> Yes

	format: [6-9][0-9]{9}

predefined character classes
----------------------------
\\s 			space character
\\S 			except space character
\\d 			digit
\\D 			except digit
\\w 			word character (a-z,A-Z,0-9)
\\W 			except word character (spaces and special characters)

Ex:
---
Pattern p = Pattern.compile("\\s");
Pattern p = Pattern.compile("\\S");
Pattern p = Pattern.compile("\\d");
Pattern p = Pattern.compile("\\D");
Pattern p = Pattern.compile("\\w");
Pattern p = Pattern.compile("\\W");
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class ExceptWordCharacterRegExpres {
	public static void main(String[] args) {
	
Pattern p=Pattern.compile("\\W");//regular expression format
Matcher m=p.matcher("ab c$123#i Jk^45 6*pQr @ wXYz");//target String

int c=0;

while(m.find())
{
	System.out.println(m.start()+"==============>"+m.end());
	c++;
}
		System.out.println(c);
	}
	
	
}
