package com.app.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultipleAMatching {

	public static void main(String[] args) {
		 Pattern p=Pattern.compile("a{3,6}");
		 
		 Matcher m=p.matcher("abaabaaabaaaabaaaaabaaaaaab");
		 
		 int c=0;
		 
		 while(m.find())
		 {
			 System.out.println(m.start()+" "+m.end()+" "+m.group());
			 c++;
		 }
				 
				 System.out.println(c);
	}
}
