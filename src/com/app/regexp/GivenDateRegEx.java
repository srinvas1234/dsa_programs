package com.app.regexp;

import java.util.Scanner;

public class GivenDateRegEx {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter date value");
		String s=sc.nextLine();
		System.out.println(s.matches("[0-3][0-9]-[0-1][012]-202[0-9]"));//04-01-2023
	}
}
