package com.app.regexp;

import java.util.Scanner;

public class BikeRegistration {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter your bike num: ");
		String s=sc.nextLine();
		
		System.out.println(s.matches("TS[0-2][0-9][A-Z]{1}[0-9]{4}"));
	}
}
