package com.app;

import java.util.Scanner;

public class PrimeNumberExample {

	
	public static boolean isPrimeNumber1(int n)
	{
		int fact=0;
		for(int i=1;i<=n;i++)
		{
			if(n%i==0)
			{
				fact++;
			}
			
		}
		return fact==2;
	}
	
	public static boolean isPrimeNumber2(int n,int i)
	{
		if(i==1)
		{
			return true;
		}else if(n%i==0) {
			return false;
		}else {
			return isPrimeNumber2(n, --i);
		}
	}
	
	
}
class Test10{
	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number");
int n=sc.nextInt();
System.out.println(PrimeNumberExample.isPrimeNumber1(n)?"Yes":"No"); //100 iteration
System.out.println((PrimeNumberExample.isPrimeNumber2(n,n/2))?"Yes":"No");//1 Iteration

	}

}
