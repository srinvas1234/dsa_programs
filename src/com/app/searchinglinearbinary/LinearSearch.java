package com.app.searchinglinearbinary;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class LinearSearch {

	public static int linearSearch(int a[],int key)
	
	{
		int i;
		
		for(i=0;i<a.length;i++)
		{
			if(key==a[i])
			{
				return i;
			}
		}
		return -1;
	}
	
	
	
}
class Test
{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		Random r=new Random();
		int a[]=new int[5];
		
		for(int i=0;i<a.length;i++)
		{
			a[i]=r.nextInt(10);
		}
		
		System.out.println("Array Elements: "+Arrays.toString(a));
		System.out.println("Enter key element");
		int key=sc.nextInt();
		System.out.println(LinearSearch.linearSearch(a,key));
		
	}
}
