package com.app;

import java.util.Scanner;

public class NaturalNumbers {

	
	public static int naturalLoop(int n)
	{
		int sum=0;
		for(int i=0;i<=n;i++)
		{
			sum=sum+i;
		}
		return sum;
	}
	public static int naturalRecursion(int n)
	{
		if(n==0)
		{
			return 0;
		}
else {
 return n+naturalRecursion(n-1);
}
	}
	
	
	public static int naturalFormula(int n)
	{
		return n*(n+1)/2;
	}
	
	
	
}
class Test7{
	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number: ");
int n=sc.nextInt();
System.out.println(NaturalNumbers.naturalLoop(n));
System.out.println(NaturalNumbers.naturalRecursion(n));
System.out.println(NaturalNumbers.naturalFormula(n));
	}

}
