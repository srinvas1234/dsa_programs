package com.app;

import java.util.Scanner;

public class Max_Numberab {

	public static int max1(int a,int b)
	{
		return (a>b)?a:b;
	}
	
	public static int max2(int a,int b)
	{
		return Math.max(a, b);
	}
	
	
	
}
class Test2
{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first Number");
		int a=sc.nextInt();
		System.out.println("Enter the second Number");
		int b=sc.nextInt();
		System.out.println(Max_Numberab.max1(a,b));
		System.out.println(Max_Numberab.max2(a,b));
	}
}