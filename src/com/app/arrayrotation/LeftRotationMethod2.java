package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotationMethod2 {
	static void leftRotation(int a[],int r){
		int i,n=a.length;
		int temp[] = new int[n];

		for(i=0;i<n;i++)
			temp[i] = a[(i+r)%n];

		for(i=0;i<n;i++)
			a[i] = temp[i];		
	}
}
class Test3435436 
{
	public static void main(String[] args) 
	{
		Scanner obj = new Scanner(System.in);

		int a[] = {1, 2, 3, 4, 5};

		System.out.println("Enter number of rotations:");
		int r = obj.nextInt();

		System.out.println("Before rotating array ===> "+Arrays.toString(a));
		LeftRotationMethod2.leftRotation(a,r);
		System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));		
	}
}