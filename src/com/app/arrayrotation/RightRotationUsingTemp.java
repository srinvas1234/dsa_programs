package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationUsingTemp {
	static void rightRotation(int a[],int r){
		int i,j,temp;
		r=r%a.length;
		for(i=0;i<r;i++)
		{
			temp = a[a.length-1];
			for(j=a.length-1;j>0;j--)
				a[j] = a[j-1];
			a[0]=temp;
		}
	}
}
class Test676732{
	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);

		int a[] = {1, 2, 3, 4, 5};

		System.out.println("Enter number of rotations:");
		int r = obj.nextInt();

		System.out.println("Before rotating array ===> "+Arrays.toString(a));
		LeftRotationUsingTemp.LeftRotation(a,r);
		System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));	
	}
}