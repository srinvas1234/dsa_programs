package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationMethod2 {
	static void rightRotation(int a[],int r){
		int i,n=a.length;
		int temp[] = new int[n];

		for(i=0;i<n;i++)
			temp[(i+r)%n] = a[i];

		for(i=0;i<n;i++)
			a[i] = temp[i];		
	}
}

class Test434 
{
	public static void main(String[] args) 
	{
		Scanner obj = new Scanner(System.in);

		int a[] = {1, 2, 3, 4, 5};

		System.out.println("Enter number of rotations:");
		int r = obj.nextInt();

		System.out.println("Before rotating array ===> "+Arrays.toString(a));
		RightRotationMethod2.rightRotation(a,r);
		System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));		
	}
}
