package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotation {

	public static void rightRotation(int a[],int r)
	{
		int i,j,prev,temp;
		
		r=r%a.length;
		
		for(i=0;i<r;i++)
		{
			prev=a[a.length-1];
			for(j=0;j<a.length;j++)
			{
				temp=a[j];
				a[j]=prev;
				prev=temp;
			}
		}
		
		
		
	}
	
	
	
	
	
}
class Test565233{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int a[]= {1,2,3,4,5};
		
		System.out.println("Enter number of rotations: ");
		int r=sc.nextInt();
		
		System.out.println("Before rotating array===>"+Arrays.toString(a));
		
		RightRotation.rightRotation(a,r);
		
		System.out.println("After '"+r+"' rotating array===>"+Arrays.toString(a));
		
	}
}
