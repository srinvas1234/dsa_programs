package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotation {

	public static void leftRotation(int a[],int r)
	{
		int i,j,temp,prev;
		
		r=r%a.length;
		
		for(i=0;i<r;i++)
		{
			prev=a[0];
			
			for(j=a.length-1;j>=0;j--)
			{
				temp=a[j];
				a[j]=prev;
				prev=temp;
			}
		}
		
		
	}
	
	
}

class Test5557644{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int a[]= {1,2,3,4,5};
		System.out.println("Enter number of ratations: ");
		int r=sc.nextInt();
		System.out.println("Before rotating array==>"+Arrays.toString(a));
		LeftRotation.leftRotation(a,r);
		System.out.println("After '"+r+"' rotating array==>"+Arrays.toString(a));
	}
}