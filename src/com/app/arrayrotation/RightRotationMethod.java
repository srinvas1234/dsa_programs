package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationMethod {
	static void rightRotation(int a[],int r){
		int i,j,n=a.length;
		int temp[] = new int[r];

		for(i=0;i<r;i++)
			temp[i] = a[n-r+i];

		for(i=n-r-1;i>=0;i--)
			a[i+r] = a[i];

		for(i=0;i<r;i++)
			a[i] = temp[i];
	}
}
	class Test23423 
	{
		public static void main(String[] args) 
		{
			Scanner obj = new Scanner(System.in);

			int a[] = {1, 2, 3, 4, 5};

			System.out.println("Enter number of rotations:");
			int r = obj.nextInt();

			System.out.println("Before rotating array ===> "+Arrays.toString(a));
			RightRotationMethod.rightRotation(a,r);
			System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));		
		}
}
