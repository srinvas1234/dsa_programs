package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class ToUppercaseLetterString {

	
	public static void main(String[] args) {
		
		
		String s="the quick brown fox jumps over the lazy dog";
		
		
		StringBuffer sb=new StringBuffer();
		
		System.out.println(s);
		
		StringTokenizer st=new StringTokenizer(s);
		
		while(st.hasMoreTokens())
		{
			String sss=st.nextToken();
			
			sb.append(sss.substring(0, 1).toUpperCase()+sss.substring(1));
			sb.append(" ");
			
		}
		System.out.println(sb.toString());
		
		
	}
}
