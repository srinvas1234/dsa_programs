package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringTokenizerReverse {

	public static void main(String[] args) {
		
		String str="the quick brown fox jumps over the lazy dog";
		System.out.println(str);

		StringTokenizer st = new StringTokenizer(str);
		while(st.hasMoreTokens()){
			System.out.print(new StringBuffer(st.nextToken()).reverse()+" ");
		}
		
	}
}
