package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringtokenizerExamplethree {

	public static void main(String[] args) {
		String s = "3-1-2023";
		StringTokenizer st = new StringTokenizer(s,"-");
		System.out.println(st.countTokens());//3
		while(st.hasMoreTokens()){
			System.out.println(st.nextToken());
		}
	}
}
