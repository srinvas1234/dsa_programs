package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringtokenizerExample {

	public static void main(String[] args) {
		
		String s="java is very easy programming";
		StringTokenizer st=new StringTokenizer(s);
		System.out.println(st.countTokens());
		while(st.hasMoreElements())
		{
			System.out.println(st.nextToken());
		}
	}
}
