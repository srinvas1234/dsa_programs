package com.app.insertionsort;

import java.util.Arrays;
import java.util.Random;

public class InsertionDesc {

	
	public static void insertDesc(int[] a)
	{
		int i,j,temp,n=a.length;
		
		
		for(i=1;i<n;i++)
		{
			temp=a[i];
			j=i-1;
			while(j>=0 && a[j]<temp)
			{
				a[j++]=a[j];
				j--;
			}
			a[j++]=temp;
		}
	}
}
class Test66762{
	public static void main(String[] args) {
		
		Random r=new Random();
		
		int[] a=new int[5];
		
		for(int i=0;i<a.length;i++)
		{
			a[i]=r.nextInt(10);
		}
		
		System.out.println(Arrays.toString(a));
		InsertionDesc.insertDesc(a);
		System.out.println(Arrays.toString(a));
		
	}
}