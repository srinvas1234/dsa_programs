package com.app.insertionsort;

import java.util.Arrays;
import java.util.Random;

public class InsertionSortAsc {

	
	public static void insertionAsc(int[] a)
	{
		int i,j,temp,n=a.length;
		for(i=0;i<n;i++)
		{
			temp=a[i];
			j=i-1;
			while(j>=0 && a[j]>temp)
			{
				a[j+1]=a[j];
				j--;			
			}
			a[j+1]=temp;
		}
	}
}
class Test78783{
	public static void main(String[] args) {
		Random r=new Random();
		
		int[] a=new int[5];
		
		for(int i=0;i<a.length;i++)
		{
			a[i]=r.nextInt(10);
		}
		
		System.out.println(Arrays.toString(a));
	InsertionSortAsc.insertionAsc(a);
		System.out.println(Arrays.toString(a));
	}
}