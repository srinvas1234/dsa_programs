package com.app.quicksort;

import java.util.Arrays;
import java.util.Random;

public class QuickSortDesc {

	static void quickSortDesc(int[] a,int lIndex,int rIndex)
	{
		if(lIndex>=rIndex)//base condition
			return;

		int pivot, lp, rp, temp;

		pivot = a[rIndex];
		lp = lIndex;
		rp = rIndex;

		while(lp<rp){
			while(a[lp]>=pivot && lp<rp)
				lp++;
			while(a[rp]<=pivot && lp<rp)
				rp--;
			temp = a[lp];
			a[lp]=a[rp];
			a[rp]=temp;
		}

		temp = a[lp];
		a[lp] = a[rIndex];
		a[rIndex] = temp;

		quickSortDesc(a,lIndex,lp-1);
		quickSortDesc(a,lp+1,rIndex);
	}
}

class Test8983 
{
	public static void main(String[] args) 
	{
		Random r = new Random();
		int[] a = new int[10];

		for(int i=0;i<a.length;i++)
		{
			a[i] = r.nextInt(100);
		}

		System.out.println("Before Sorting====> "+Arrays.toString(a));
		QuickSortDesc.quickSortDesc(a,0,a.length-1);
		System.out.println("After Sorting====> "+Arrays.toString(a));
	}
}