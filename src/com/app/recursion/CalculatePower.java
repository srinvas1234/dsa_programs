package com.app.recursion;

import java.util.Scanner;

public class CalculatePower {

	
	public static int power(int a,int b)
	{
		if(b>=1)
		{
			return a*power(a,b-1);
		}
		else {
			return 1;
		}
	}
}
class Test6623{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter first Number");
		int a=sc.nextInt();
		System.out.println("enter second Number");
		int b=sc.nextInt();
		
		System.out.println(CalculatePower.power(a,b));
		
	}
}
