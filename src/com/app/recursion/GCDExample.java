package com.app.recursion;

import java.util.Scanner;

public class GCDExample {

	
	public static int gcd(int a,int b)
	{
		while(a!=b)
		{
			if(a>b) //3>4
			{
				return gcd(a-b,b);
			}else {
				return gcd(a,b-a);
			}
		}
		return a;
	}
	
}
class Test67677{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the n1 value: ");
		int n1=sc.nextInt();
		System.out.println("Enter the n2 value: ");
		int n2=sc.nextInt();
		System.out.println(GCDExample.gcd(n1,n2));
	}
}