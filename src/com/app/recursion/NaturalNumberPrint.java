package com.app.recursion;

import java.util.Scanner;

public class NaturalNumberPrint {

	public static void print(int n)
	{
		if(n>=1)
		{
			System.out.println(n+" ");
			print(n-1);
		}
	}

}
class Test666{
	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the natural Number: ");
int n=sc.nextInt();

NaturalNumberPrint.print(n);

	}

}
