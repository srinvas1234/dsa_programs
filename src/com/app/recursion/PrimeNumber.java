package com.app.recursion;

import java.util.Scanner;

public class PrimeNumber {

	
	public static boolean isPrime(int n,int i)
	{
		if(i==1)
		{
			return true;
		}
		else if (n%i==0) {
			return false;
		}else {
			return isPrime(n, --i);
		}
	}
	
}
class Test6332{
	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number: ");
int n=sc.nextInt();
System.out.println(PrimeNumber.isPrime(n,n/2)?"Yes":"No");
	}

}
