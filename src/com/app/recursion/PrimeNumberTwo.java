package com.app.recursion;

import java.util.Scanner;

public class PrimeNumberTwo {

	public static boolean isPrime(int n,int i)
	{
		if(i==1)
		{
			return true;
		}
		else if (n%i==0) {
			return false;
		}
		else {
			return isPrime(n, --i);
		}
	}
}
	class Test787{
	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number: ");
int n=sc.nextInt();
//System.out.println(PrimeNumberTwo.isPrime(n,n/2)?"Yes":"No");

for(int i=2;i<=n;i++)
{
	System.out.println(i+"====>"+PrimeNumber.isPrime(i, i/2));
}


	}

}
