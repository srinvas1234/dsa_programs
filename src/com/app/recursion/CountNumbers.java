package com.app.recursion;

import java.util.Scanner;

public class CountNumbers {


	static int count=0;
	public static int count(int n)
	{
		if(n!=0)
		{
			count++;
			count(n/10);
		}
		return (count!=0?count:1);
	}
	
	
}
class Test65767{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		int n=sc.nextInt();
		System.out.println(CountNumbers.count(n));
	}
}