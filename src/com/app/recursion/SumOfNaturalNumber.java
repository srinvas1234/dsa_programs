package com.app.recursion;

import java.util.Scanner;

public class SumOfNaturalNumber {

	
	public static int sum(int n)
	{
		if(n==1)
		{
			return 1;
		}
		else {
			return n+sum(n-1);
		}
	}
	
	
}
class Test778{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number: ");
		int n=sc.nextInt();
		System.out.println(SumOfNaturalNumber.sum(n));
	}
}
