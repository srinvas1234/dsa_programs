package com.app.recursion;

import java.util.Scanner;

public class LCMExample {

	static int com = 1;
	static int lcm(int n1,int n2)
	{
		if(com%n1==0 && com%n2==0)
			return com;
		com++;
		return lcm(n1,n2);		
	}
}
class Test78337{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the n1 number: ");
		int n1=sc.nextInt();
		System.out.println("Enter the n2 number: ");
		int n2=sc.nextInt();
		System.out.println(LCMExample.lcm(n1,n2));
		
	}
}