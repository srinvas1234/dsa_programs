package com.app.recursion.strings;

import java.util.Scanner;

public class TowersOfHanoi {

	static void towersOfHanoi(int n,String src,String temp,String dest)
	{
		if(n==1)
		{
			System.out.println("Move the Disk "+n+" from "+src+" to "+dest);
			return;
		}
		towersOfHanoi(n-1,src,dest,temp);
		System.out.println("Move the Disk "+n+" from "+src+" to "+dest);
		towersOfHanoi(n-1,temp,src,dest);
	}
}
class Test5565{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no of disks: ");

		int n=sc.nextInt();
		TowersOfHanoi.towersOfHanoi(n,"S","T","D");
	}
}