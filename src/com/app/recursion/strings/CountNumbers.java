package com.app.recursion.strings;

import java.util.Scanner;

public class CountNumbers {

	
	public static int count(String s,int ch,int index)
	{
		if(index<0)
		{
			return 0;
		}else if (s.charAt(index)=='x') {
			return 1+count(s, ch, index-1);
		}else {
			return count(s, ch, index-1);
		}
	}
	
	
}
class Test66{
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s=sc.nextLine();
		System.out.println(CountNumbers.count(s,'x',s.length()-1));
	}
}