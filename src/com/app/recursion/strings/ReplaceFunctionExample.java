package com.app.recursion.strings;

import java.util.Scanner;

public class ReplaceFunctionExample {

	
	public static String replace(String s,int index)
	{
		if(index<0)
		{
			return "";
		}
		else if (s.charAt(index)=='x') {
			return replace(s, index-1)+"y";
		}else {
			return replace(s, index-1)+s.charAt(index);
		}
	}
}

class Test5563{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter any String: ");
		String s=sc.nextLine();
		System.out.println(ReplaceFunctionExample.replace(s,s.length()-1));
	}
}