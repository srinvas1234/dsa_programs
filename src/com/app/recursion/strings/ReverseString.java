package com.app.recursion.strings;

import java.util.Scanner;

public class ReverseString {

	public static String revStr(String s)
	{
		
	if(s==null||s.length()<=1)
	{
		return s;
	}
	else {
		return revStr(s.substring(1))+s.charAt(0);
	}
}
}
class Test67633{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s=sc.nextLine();
		System.out.println(ReverseString.revStr(s));
	}
}