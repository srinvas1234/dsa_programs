package com.app.recursion.strings;

import java.util.Scanner;

public class RemoveCharacter {


	public static String strremove(String s,int index)
	{
		if(index<0)
		{
			return "";
		}else if (s.charAt(index)=='x') {
			return strremove(s, index-1);
		}
		else {
			return strremove(s, index-1)+s.charAt(index);
		}
	}
	
	

	
}

class Test66672{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s=sc.nextLine();
		System.out.println(RemoveCharacter.strremove(s,s.length()-1));
	}
}