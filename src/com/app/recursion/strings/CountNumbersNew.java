package com.app.recursion.strings;

import java.util.Scanner;

public class CountNumbersNew {

	public static int count(String s,char ch,int index)
	{
		if(index<0)
		{
			return 0;
		}else if (s.charAt(index)==ch) {
			return 1+count(s, ch, index-1);
		}else {
			return count(s, ch, index-1);
		}
	}
	
	
	
}
class Test{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter any String: ");
		String s=sc.nextLine();
		System.out.println(CountNumbersNew.count(s,'x',s.length()-1));
	}
}