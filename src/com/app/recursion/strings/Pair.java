package com.app.recursion.strings;

import java.util.Scanner;

public class Pair {

	public static boolean validateOrNot(String s,int i,int j)
	{
		if(i>j)//4>3 
		{
			return true;
		}else if (s.charAt(i)=='(' && s.charAt(j)==')') {
			return validateOrNot(s, i+1, j-1);
		}
		else {
			return false;
		}
	}
	
	
	
}
class Test7878{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s=sc.nextLine();
		System.out.println(Pair.validateOrNot(s,0,s.length()-1));
		}
}
