package com.app.recursion.strings;

import java.util.Scanner;

public class ReplaceNewStringPiExample {

	public static String replacenew(String s,int index)
	{
		if(index<1)
		{
			return s.substring(0,index+1);
		}else if (s.substring(index-1, index+1).equals("pi")) {
			return replacenew(s, index-2)+"3.147";
		}else {
			return replacenew(s, index-1)+s.charAt(index);
		}
	}
}
class Test89893{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s=sc.nextLine();
		System.out.println(ReplaceNewStringPiExample.replacenew(s,s.length()-1));
	}
}