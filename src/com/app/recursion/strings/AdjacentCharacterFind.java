package com.app.recursion.strings;

import java.util.Scanner;

public class AdjacentCharacterFind {

	
	
	public static String news(String s,int index)
	{
		if(index<1)
		{
			return ""+s.charAt(index);
		}else
		{
			return news(s, index-1)+"*"+s.charAt(index);		}
	}
	
	
	
}
class Test083{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s=sc.nextLine();
		System.out.println(AdjacentCharacterFind.news(s,s.length()-1));
	}
}