package com.app.recursion;

import java.util.Scanner;

public class FactorialExample {

	
	public static int fact(int n)
	{
		if(n==0)
		{
			return 1;
		}
		else {
			return n*fact(n-1);
		}
	}
	
}
class Test933{
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the N value: ");
		int n=sc.nextInt();
		System.out.println(FactorialExample.fact(n));
	}
}
