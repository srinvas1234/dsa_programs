package com.app.recursion;

import java.util.Scanner;

public class DecimalToBinary {

	
	public static int convert(int n)
	
	{
		if(n==0)
		{
			return 0;
		}else {
			return (n%2)+(10*convert(n/2));
		}
	}
	
	
	
}
class Test6989{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the number :");
		int n=sc.nextInt();
		System.out.println(DecimalToBinary.convert(n));
	}
}