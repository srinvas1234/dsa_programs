package com.app.recursion;

import java.util.Scanner;

public class PrimeNumberThree {

	public static boolean isPrime(int n,int i)
	{
		if(i==1)
		{
			return true;
		}else if (n%i==0) {
			return false;
		}
		else {
			return isPrime(n, --i);
		}
	}
	
	
}
class Test565673{
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		int n=sc.nextInt();
		
		for(int i=2;i<=n;i++) {
			if(PrimeNumberThree.isPrime(i,i/2)) {
				System.out.print(i+",");
			}
		}
	}
}
