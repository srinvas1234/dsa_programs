package com.app.recursion;

import java.util.Scanner;

public class FibonnaciSeries {

	
	public static int fib(int n)
	{
		if(n==0||n==1)
			
		{
			return n;
		}else {
			return fib(n-1)+fib(n-2);
		}
	}

}

class Test77{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		int n=sc.nextInt();
		System.out.println(FibonnaciSeries.fib(n));
	}
}