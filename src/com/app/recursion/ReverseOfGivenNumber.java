package com.app.recursion;

import java.util.Scanner;

public class ReverseOfGivenNumber {

	
	public static int reverse(int n,int length)
	{
		if(n==0)
		{
			return 0;
		}else {
			return ((n%10)*(int)Math.pow(10, length-1))+reverse(n/10, --length);
		}
	}
	
	
	
}
class Test56767{
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number: ");
		String s=sc.nextLine();
		System.out.println(ReverseOfGivenNumber.reverse(Integer.parseInt(s),s.length()));
		
	}
}