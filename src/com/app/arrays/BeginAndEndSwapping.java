package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BeginAndEndSwapping {
public static void main(String[] args) {
	
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Array size: ");
	int n=sc.nextInt();
	
	int[] a=new int[n];
	System.out.println("Enter the '"+n+"' elements");
	
	for(int i=0;i<n;i++)
	{
		a[i]=sc.nextInt();
	}
	
	
	System.out.println("Array Before elements: "+Arrays.toString(a));
	
	int begin,end;
	
	System.out.println("Enter begin location");
	begin=sc.nextInt();
	System.out.println("Enter end location");
	end=sc.nextInt();
	
	int i,j,t;
	
	
	for(i=begin;i<end;i++)
	{
		for(j=i+1;j<end;j++)
		{
			if(a[i]>a[j]) {
				t=a[i];
				a[i]=a[j];
				a[j]=t;
			}
		}
	}
	
	
	
	
	System.out.println("Array After elements: "+Arrays.toString(a));
	
	
}
}
