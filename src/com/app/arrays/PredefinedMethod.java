package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class PredefinedMethod {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Array Size: ");
	int n=sc.nextInt();
	
	int[] a=new int[n];
	System.out.println("Enter '"+n+"' elements");
	
	for(int i=0;i<n;i++)
	{
		a[i]=sc.nextInt();
	}
	
	System.out.println(" Before sorting: "+Arrays.toString(a));
	Arrays.sort(a);
	System.out.println(" After sorting: "+Arrays.toString(a));
}
}
