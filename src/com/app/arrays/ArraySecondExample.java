package com.app.arrays;

public class ArraySecondExample {

	public static void main(String[] args) {

		int a[]=new int[3];
		System.out.println(a[0]);
		System.out.println(a[1]);
		System.out.println(a[2]);
		
		a[0]=111;
		a[1]=121;
		a[2]=131;
		System.out.println(a[0]);
		System.out.println(a[1]);
		System.out.println(a[2]);
		//System.out.println(a[3]);//java.lang.ArrayIndexOutOfBoundsException: 3
	}

}
