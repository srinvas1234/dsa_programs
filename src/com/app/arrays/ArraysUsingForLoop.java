package com.app.arrays;

import java.util.Scanner;

public class ArraysUsingForLoop {

	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);

System.out.println("Enter Array Size: ");
int n=sc.nextInt();

int a[]=new int[n];
System.out.println("Enter Array  '"+n+"' Elements");

for(int i=0;i<n;i++) {
	a[i]=sc.nextInt();
}
System.out.println("Using while loop: ");

int index=0;
while(index<a.length) {
	System.out.println(a[index]);
	index++;
}
System.out.println("Using for loop: ");

for(int i=0;i<a.length;i++)
{
	System.out.println(a[i]);
}


System.out.println("Using foreach loop: ");

for (int i : a) {
	System.out.println(i);
}

	}

}
