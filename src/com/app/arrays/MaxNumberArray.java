package com.app.arrays;

import java.util.Scanner;

public class MaxNumberArray {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size: ");
	int n=sc.nextInt();
	
	int a[]=new int[n];
	
	System.out.println("Enter '"+n+"' elements");
	
	for(int i=0;i<n;i++)
	{
		a[i]=sc.nextInt();
	}
	
	int max;
	max=a[0];
	
	for(int i=1;i<n;i++)
	{
		if(max<a[i])
		{
			max=a[i];
		}
	}
	
	System.out.println("Max Number: "+max);
	
	
	
}
}
