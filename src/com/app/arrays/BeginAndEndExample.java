package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BeginAndEndExample {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Array Size: ");
	int n=sc.nextInt();
	
	int[] a=new int[n];
	
	System.out.println("Enter Array elements: '"+n+"'");
	
	for(int i=0;i<n;i++)
	{
		a[i]=sc.nextInt();
	}
	
	System.out.println("Array before sort: "+Arrays.toString(a));
	
	System.out.println("Enter begin location: ");
	int begin=sc.nextInt();
	System.out.println("Enter end location: ");
	int end=sc.nextInt();
	Arrays.sort(a,begin,end);
	
	System.out.println("Array After sort: "+Arrays.toString(a));
}
}
