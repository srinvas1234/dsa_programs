package com.app.arrays;

import java.util.Scanner;

public class MinNumber {
public static void main(String[] args) {
	
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter Array Size: ");
	int n=sc.nextInt();
	
	int a[]=new int[n];
	
	
	System.out.println("Enter an Array elemements '"+n+"' ");
	
	for(int i=0;i<n;i++) {
		a[i]=sc.nextInt();
	}
	
	int min;
	min=a[0];
	for(int i=1;i<n;i++)
	{
		if(min>a[i])
		{
			min=a[i];
		}
	}
	System.out.println("Min Number: "+min);
	
}
}
