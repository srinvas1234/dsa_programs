package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class ArraySortDescendingOrder {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array size: ");
		int n=sc.nextInt();
		
		int a[]=new int[n];
		
		System.out.println("Enter '"+n+"' elements");
		for(int i=0;i<n;i++)
			
		{
			a[i]=sc.nextInt();
		}
		
		System.out.println("Array Before sorting: "+Arrays.toString(a));
		
		int i,j,temp;
		
		for(i=0;i<n;i++)
		{
			for(j=i+1;j<n;j++)
			{
				if(a[i]<a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		
		
		System.out.println("Array After sorting: "+Arrays.toString(a));
	}
}
