package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class FormulaArray {

	public static void main(String[] args) {

		
		/*
		 * 1st min element = a[1-1]
		 *  2nd min element = a[2-1] 
		 *  3rd min element = a[3-1]
		 * kth min element = a[k-1]
		 *  . . 1st max element = a[n-1]
		 *   2nd max element =
		 * a[n-2] 3rd max element = a[n-3]
		 *  kth max element = a[n-k]
		 */
			
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Array size: ");
		
		int n=sc.nextInt();
		
		int a[]=new int[n];
		System.out.println("Enter '"+n+"' elements");
		
		for(int i=0;i<n;i++)
		{
			a[i]=sc.nextInt();
		}
		
		
		System.out.println("Array Before sorting: "+Arrays.toString(a));
		Arrays.sort(a);
		System.out.println("Array After sorting: "+Arrays.toString(a));
		
		
		System.out.println("1st min element="+a[1-1]);
		System.out.println("2nd min element="+a[2-1]);
		System.out.println("3rd min element="+a[3-1]);
		
		
		System.out.println("1st max element="+a[n-1]);
		System.out.println("2nd max element="+a[n-2]);
		System.out.println("3rd max element="+a[n-3]);
	}

}
