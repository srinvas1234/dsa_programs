package com.app.selectionsort;

import java.util.Arrays;
import java.util.Random;

public class SelectionSortDesc {

	
	public static void selectionDesc(int[] a)
	{
		int i,j,temp,max,n=a.length;
		
		for(i=0;i<n-1;i++)
		{
			max=i;
			
			for(j=i+1;j<n;j++)
			{
				if(a[j]>a[max])
				max=j;
				}
			
			if(max!=i)
			{
				temp=a[i];
				a[i]=a[max];
				a[max]=temp;
			}	
		}	
	}
		
}
class Test67778{
	public static void main(String[] args) {
		Random r=new Random();
		
		
		int[] a=new int[5];
		
		for(int i=0;i<a.length;i++)
		{
			a[i]=r.nextInt(10);
		}
		System.out.println(Arrays.toString(a));
		SelectionSortDesc.selectionDesc(a);
		System.out.println(Arrays.toString(a));
		
		
		
		
		
		
	}
}