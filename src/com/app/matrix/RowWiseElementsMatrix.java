package com.app.matrix;

import java.util.Scanner;

public class RowWiseElementsMatrix {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		
		int[][] a=new int[3][3];
		
		System.out.println("Enter Array Elements");
		int i,j,sum=0;
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		System.out.println("Enter Array Elements are");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		
		for(i=0;i<3;i++)
		{
			sum=0;
			for(j=0;j<3;j++)
			{
				sum=sum+a[i][j];
			}
			System.out.println(i+" row sum="+sum);
		}
		
		
		
		
	}

}
