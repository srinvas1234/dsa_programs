package com.app.matrix;

import java.util.Scanner;

public class ScalarMatrix {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		int[][] a=new int[3][3];
		
		int i,j,t;
		System.out.println("Matrix: ");
		
		for(i=0;i<3;i++){
			for(j=0;j<3;j++){
				a[i][j] = sc.nextInt();
			}
		}
		System.out.println("Enter matrix elements:");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		System.out.println("Enter multiplier");
		int n=sc.nextInt();

		System.out.println("updated matrix elements are:");
		for(i=0;i<3;i++){
			for(j=0;j<3;j++){
				System.out.print((n*a[i][j])+" ");
			}
			System.out.println();
		}
	}
}
