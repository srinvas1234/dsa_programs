package com.app.matrix;

import java.util.Scanner;

public class SymmetricMatrix {

		public static void main(String[] args) throws Exception
		{
			Scanner obj = new Scanner(System.in);

			int a[][] = new int[3][3];//3-rows and 3-cols

			int i,j,t;

			System.out.println("Enter matrix elements:");
			for(i=0;i<3;i++){
				for(j=0;j<3;j++){
					a[i][j] = obj.nextInt();
				}
			}

			System.out.println("matrix elements are:");
			for(i=0;i<3;i++){
				for(j=0;j<3;j++){
					System.out.print(a[i][j]+" ");
				}
				System.out.println();
			}
			boolean flag=true;
			
			for(i=0;i<3;i++)
			{
				for(j=0;j<3;j++)
				{
					if(i==j && a[i][j]!=1)
					{
						flag=false;
					}
					
					if(i!=j && a[i][j]!=0)
					{
						flag=false;
						break;
					}
					
				}
			}
			
			
			System.out.println(flag);
			
			

	}

}
