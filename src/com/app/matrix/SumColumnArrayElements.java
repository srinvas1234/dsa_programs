package com.app.matrix;

import java.util.Scanner;

public class SumColumnArrayElements {

	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		int[][] a=new int[3][3];
		
		int i,j,sum=0;
		System.out.println("Enter array elemens");
		
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		
		System.out.println("Enter array Matrix elements: ");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		
		for(i=0;i<3;i++)
		{
			sum=0;
			for(j=0;j<3;j++)
			{
				sum=sum+a[j][i];
			}
			System.out.println(i+" column sum="+sum);

		}
		
				
		
		
	}
}
