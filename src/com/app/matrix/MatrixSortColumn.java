package com.app.matrix;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixSortColumn {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		int[][] a=new int[3][3];
		System.out.println("Enter Matrix Array: ");
		
	int i,j;
	
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			a[i][j]=sc.nextInt();
		}
	}
	System.out.println(" Matrix elements: ");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
				
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		int b[][]=new int[3][3];
		
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				b[i][j]=a[j][i];
			}
		}
		
		for(i=0;i<3;i++)
		{
			Arrays.sort(b[i]);
		}
		
		System.out.println("Updated Matrix elements: ");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				System.out.print(b[j][i]+" ");
			}
			System.out.println();
		}
		
	}
}
