package com.app.matrix;

import java.util.Scanner;

public class ColumnWiseMinMaxElements {

	
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		int[][] a=new int[3][3];
		int i,j;
		System.out.println("Enter Array Matrix: ");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		
		
		System.out.println("Enter Array Matrix elements: ");
		
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		
		int min;
		
		for(i=0;i<3;i++)
		{
			min=a[0][i];
			for(j=0;j<3;j++)
			{
				if(min>a[j][i])
				{
					min=a[j][i];
				}
			}
			System.out.println(i+" min elements in column element "+min);
		}
		
int max;
		
		for(i=0;i<3;i++)
		{
			max=a[0][i];
			for(j=0;j<3;j++)
			{
				if(max<a[j][i])
				{
					max=a[j][i];
				}
			}
			System.out.println(i+" min elements in column element "+max);
		}
		
		
		
	}
}
