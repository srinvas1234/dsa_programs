package com.app.matrix;

import java.util.Scanner;

public class MinMaxMatrixElement {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		int[][] a=new int[3][3];
		
		int i,j;
		System.out.println("Enter array Matrix");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		System.out.println("Enter array Matrix elements");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		int min=a[0][0];
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				if(min>a[i][j])
				{
					min=a[i][j];
				}
			}
		}
		
		int max=a[0][0];
		
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				if(max<a[i][j])
				{
					max=a[i][j];
				}
			}
		}
		
		System.out.println("MIN Element: "+min);
		System.out.println("MAX Element: "+max);
		
		
		
		
	}
}
