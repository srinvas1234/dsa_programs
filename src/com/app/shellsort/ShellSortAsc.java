package com.app.shellsort;

import java.util.Arrays;
import java.util.Random;

public class ShellSortAsc {

	
	public static void shellAsc(int a[],int n)
	{
		int gap,i,j,temp;
		for(gap=n/2;gap>=1;gap=gap/2)
		{
			for(j=gap;j<n;j++)
			{
				for(i=j-gap;i>=0;i=i-gap)
				{
					if(a[i+gap]>a[i])
						break;
					else
					{
						temp = a[i+gap];
						a[i+gap] = a[i];
						a[i] = temp;
					}
				}
			}
		}	
	}	
}
	
	
	

class Test55673{
	public static void main(String[] args) {

		Random r=new Random();
		
		
		int[] a=new int[5];
		
		for(int i=0;i<a.length;i++)
		{
			a[i]=r.nextInt(10);
		}
		
		
		System.out.println(Arrays.toString(a));
		ShellSortAsc.shellAsc(a,a.length);
		System.out.println(Arrays.toString(a));
	}

}
