package com.app.binarysearch;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearchIteration {

	public static int binarySearchIteration(int[] a,int key)
	{
		int low,high,mid;
		low=0;
		high=a.length-1;
		while(low<=high)
		{
			mid=(low+high)/2;
			if(key==a[mid])
				return mid;
		else if (key<a[mid])
				high=mid-1;
			else
				low=mid+1;
		}
		return -1;
	}	
}
class Test676733{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int a[]= {21,12,13,111,21,12};
		System.out.println("Array elements: "+Arrays.toString(a));
		Arrays.sort(a);
		System.out.println("Arrays Elements:"+Arrays.toString(a));
		System.out.println("Enter key elements");
		int key=sc.nextInt();
		System.out.println(BinarySearchIteration.binarySearchIteration(a,key));
		
		
		
		
	}
}