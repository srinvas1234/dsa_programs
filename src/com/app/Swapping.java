package com.app;

import java.util.Scanner;

public class Swapping {

	
	public static void swap1(int a,int b) {
		System.out.println("Before swapping '"+a+"' and '"+b+"' ");
		int temp;
		temp=a;
		a=b;
		b=temp;
		System.out.println("After swapping '"+a+"' and '"+b+"' ");
	}
	
	public static void swap2(int a,int b) {
		System.out.println("Before swapping '"+a+"' and '"+b+"' ");
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swapping '"+a+"' and '"+b+"' ");
	}
	
	
	public static void swap3(int a,int b) {
		System.out.println("Before swapping '"+a+"' and '"+b+"' ");
		a=a*b;
		b=a/b;
		a=a/b;
		
		System.out.println("After swapping '"+a+"' and '"+b+"' ");
	}
	
	public static void swap4(int a,int b) {
		System.out.println("Before swapping '"+a+"' and '"+b+"' ");
		
		a=a^b;
		b=a^b;
		a=a^b;
		
		System.out.println("After swapping '"+a+"' and '"+b+"' ");
	}
	
	public static void swap5(int a,int b) {
		System.out.println("Before swapping '"+a+"' and '"+b+"' ");
		
		a=a+b-(b=a);
		System.out.println("After swapping '"+a+"' and '"+b+"' ");
	}
}
class Test3{
	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter first number: ");
int a=sc.nextInt();
System.out.println("Enter second number: ");
int b=sc.nextInt();

Swapping.swap1(a,b);
Swapping.swap2(a,b);
Swapping.swap3(a,b);
Swapping.swap4(a,b);
Swapping.swap5(a,b);


	}

}
