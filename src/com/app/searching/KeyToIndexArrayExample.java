package com.app.searching;

import java.util.Arrays;
import java.util.Scanner;

public class KeyToIndexArrayExample {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter an Array Size: ");
	int n=sc.nextInt();
	
	int a[]=new int[n];
	
	System.out.println("Enter '"+n+"' elements");
	
	for(int i=0;i<n;i++)
	{
		a[i]=sc.nextInt();
	}
	
	System.out.println("Array Before sorting: "+Arrays.toString(a));
	
	
	System.out.println("Enter element key to search");
	
	int key=sc.nextInt();
	
	int index=-1;
	for(int i=0;i<n;i++)
	{
		if(key==a[i])
		{
			index=i;
			break;
		}
	}
	
	System.out.println("Location: "+index);
}
}
