package com.app.searching;

import java.util.Arrays;
import java.util.Scanner;

public class FindLocationUsingLowHighMid {

	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter an Array Size: ");
		
		int n=sc.nextInt();
		
		int[] a=new int[n];
		
		System.out.println("Enter the array elements : '"+n+"' ");
		
		for(int i=0;i<n;i++) {
		a[i]=sc.nextInt();
		}
		
		System.out.println("Array Before sorting: "+Arrays.toString(a));
		Arrays.sort(a);
		System.out.println("Array After sorting: "+Arrays.toString(a));
		
		
		System.out.println("Enter key to search: ");
		int key=sc.nextInt();
		
		
		
		int low,high,mid;
		
		low=0;
		high=n-1;
		
		int index=-1;
		while(low<=high)
			
		{
			mid=(low+high)/2;
			
			if(a[mid]==key)
			{
				index=mid;
				break;
			}else if (a[mid]>key) {
				high=mid-1;
			}else {
				low=mid+1;
			}
			
			
		}
			
			
		System.out.println("Location: "+index);	
			
	}
}
