package com.app.commaspace;

import java.util.StringTokenizer;

public class StringTokenizerEx {

	public static void main(String[] args) {
		
		String s="the quick brown fox jumps over the lazy dog";
		
		StringTokenizer s1=new StringTokenizer(s);
		
		while(s1.hasMoreElements()) {
			System.out.println(s1.nextToken());
		}
		
		
	}
}
